.. |label| replace:: Suche nach kundenspezifische Artikelnummern
.. |snippet| replace:: FvCustomerArticleNumbers
.. |Author| replace:: 1st Vision GmbH
.. |Entwickler| replace:: Tristan Hahner
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.3.4
.. |Version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Das Plugin ermöglicht die Suche nach kundenspezifischen Artikelnummern, sofern der Kunde eingeloggt ist.

Frontend
--------
Über den normalen Suchergebnissen in der Ajax-Suche und in der normalen Suche werden zusätzlich kundenspezifische Ergebnisse angezeigt.

Diese sind nicht besonders gekennzeichnet, da das Plugin nicht ins Template eingreift. Allerdings wird die Variable „fvIsCustomerResult“ als Teil des Result-Arrays ans Template geliefert, mit denen die Anzeige individualisiert werden kann. Jedes kundenspezifische Artikelergebnis hat diese Variable gesetzt. Auf diese Weise könnten kundenspezifische Ergebnisse gekennzeichnet werden.

Wichtig: Die Anzeige der kundenspezifischen Artikelnummern erfolgt nicht über dieses Plugin, sondern ist im Preisfindungsmodul implementiert.


Backend
-------
Installation
____________
Die kundenspezifischen Artikelnummern werden mit den kundenspezifischen Preisen über den Connector in die Tabelle fv_article_customerprices des Shops importiert. Hierzu ist die Aktivierung der kundenspezifischen Preise unerlässlich.

Konfiguration:
______________

.. image:: FvCustomerArticleNumbers1.png

:Max Ergebnisse in der Ajax-Suche: Die maximale Anzahl an dargestellten kundenspezifischen Treffern den Sofortergebnissen.

:Max Ergebnisse in normaler Suche: Die maximale Anzahl an dargestellten kundenspezifischen Treffern in der normalen Suche.

:Max Unschärfe der Suche in Prozent: Die Unschärfe eines Treffers wird mit Hilfe der Levenshtein Distanz ermittelt

`https://de.wikipedia.org/wiki/Levenshtein-Distanz <https://de.wikipedia.org/wiki/Levenshtein-Distanz>`_

Diese Distanz wird durch die Gesamtlänge der kundenspezifischen Artikelnummer des Treffers geteilt, um die prozentuale Abweichung zu erhalten.

Beispiel: 

bei Suche nach „Tor“ erscheint der Treffer „Tier“. Tier und Tor haben eine Levenshtein-Distanz von 2. Diese Distanz wird durch die Anzahl der Buchstaben des Treffers geteilt: 2 / 4 = 0.5
Somit beträgt die prozentuale Abweichung 50 %. 

:Weiterleitung auf Artikel bei direkten Treffern: Bei Aktivierung dieser Funktion, wird bei Eingabe der exakten kundenspezifischen Artikelnummer direkt auf den betreffenden Artikel weitergeleitet.

technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
keine



